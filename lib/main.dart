import 'package:flutter/material.dart';

import 'screens/root_screen.dart';
import 'services/authentication.dart';

void main() => runApp(AuthApp());

class AuthApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: RootScreen(
        auth: Auth(),
      ),
    );
  }
}
